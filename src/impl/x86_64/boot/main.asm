global start
extern long_mode_start

section .text
bits 32
start:
	mov esp, stack_top ; point to stack

	; to enable long mode we need to check if cpu supports it
	call check_multiboot
	call check_cpuid
	call check_long_mode

	; enable paging to enable long mode
	call setup_page_tables
	call enable_paging

	; load global descriptor table
	lgdt [gdt64.pointer]
	jmp gdt64.code_segment:long_mode_start

	hlt

check_multiboot:
	cmp eax, 0x36d76289 ; every compliant boot loader has this value in eax
	jne .no_multiboot ; is not equal -> no multiboot
	ret
.no_multiboot:
	mov al, "M"
	jmp error

check_cpuid:
	; check if you can flip the cpuid bit in the flags register
	pushfd ; push flags register on stack
	pop eax ; put it on eax register
	mov ecx, eax ; make copy so we can confirm flip later on
	xor eax, 1 << 21 ; xor to flip bit 21 (cpuid bit)
	push eax 
	popfd
	pushfd
	pop eax
	push ecx
	popfd 
	cmp eax, ecx ; some logic to verify that the bit doesnt flip 
	je .no_cpuid
	ret
.no_cpuid:
	mov al, "C"
	jmp error

check_long_mode:
	mov eax, 0x80000000
	cpuid ; if long mode is supportet, this command will return something greater then 0x80000000 in eax
	cmp eax, 0x80000001 ; check if thats the case
	jb .no_long_mode
	
	mov eax, 0x80000001
	cpuid ; different check for long mode, in this case cpuid will flip bit 29 if long mode
	test edx, 1 << 29 ; text if bit 29 is flipped
	jz .no_long_mode
	
	ret
.no_long_mode:
	mov al, "L"
	jmp error

setup_page_tables:
	mov eax, page_table_l3
	or eax, 0b11 ; enable present and writable flags in page table (first 12 bits are reserved for flags)
	mov [page_table_l4], eax ; put this adress in first entry of l4 table so that they're linked

	mov eax, page_table_l2
	or eax, 0b11 
	mov [page_table_l3], eax ; no need to use l1 table, we can point directly to physical memory instad which gives us 2mb 

	mov ecx, 0 ; counter

.loop:

	mov eax, 0x200000 ; 2MiB
	mul ecx
	or eax, 0b10000011 ; present, writable, huge page
	mov [page_table_l2 + ecx * 8], eax

	inc ecx ; increment counter
	cmp ecx, 512 ; checks if whole table is mapped
	jne .loop ; if not continue

	ret

enable_paging:
	; pass table location to the cpu
	mov eax, page_table_l4
	mov cr3, eax

	;enable PAE
	mov eax, cr4
	or eax, 1 << 5 ; enable PAE bit
	mov cr4, eax ; save changes in eax

	;enable long mode
	mov ecx, 0xC0000080 ; magic value
	rdmsr
	or eax, 1 << 8 ; long mode bit
	wrmsr

	; enable paging
	mov eax, cr0
	or eax, 1 << 31 ; paging bit 
	mov cr0, eax

	ret

error:
	; print "ERR: X" where X is the error code inside al
	mov dword [0xb8000], 0x4f524f45
	mov dword [0xb8004], 0x4f3a4f52
	mov dword [0xb8008], 0x4f204f20
	mov byte  [0xb800a], al
	hlt

;impl stack
section .bss
align 4096 ; align all page tables to 4kb (size of page tables)
page_table_l4:
	resb 4096
page_table_l3:
	resb 4096
page_table_l2:
	resb 4096
stack_bottom:
	resb 4096 * 4 ; reserve 4kb
stack_top:

; define global datatable to enter 64 bit mode
section .rodata
gdt64:
	dq 0 ; zero entry
.code_segment: equ $ - gdt64
	dq (1 << 43) | (1 << 44) | (1 << 47) | (1 << 53) ; code segment, + enable some flags
.pointer:
	dw $ - gdt64 - 1 ; length
	dq gdt64 ; address
